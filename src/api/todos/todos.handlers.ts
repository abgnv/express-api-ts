import { NextFunction, Request, Response, Router } from "express"
import { TodoWithID, Todos, Todo } from "./todos.model"
import { InsertOneResult } from 'mongodb'
import { ZodError } from "zod"
import { ParamsWithId } from "../../interfaces/ParamsWithId"
//<{}, Todo[]>
import { ObjectId } from 'mongodb'
export const findAll = async (req: Request, res: Response<TodoWithID[]>, next: NextFunction) => {
    try {
        const result = await Todos.find()
        const todos = await result.toArray()
        res.json(todos)
    } catch (error) {
        next(error)
    }
}

export const createOne = async (
    req: Request<{},
        InsertOneResult<Todo>,
        Todo>,
    res: Response,
    next: NextFunction
) => {
    try {
        const insertResult = await Todos.insertOne(req.body)

        if (!insertResult.acknowledged) throw new Error('Error inserting todo')

        res.status(201)

        res.json({
            _id: insertResult.insertedId,
            ...req.body
        })

    } catch (error) {
        next(error)
    }
}

export const findOne = async (req: Request<ParamsWithId, TodoWithID, {}>, res: Response<TodoWithID>, next: NextFunction) => {
    try {
        const result = await Todos.findOne({
            _id: new ObjectId
        })

        if (!result) {
            res.status(404)
            throw new Error(`Todo with id "${req.params.id}"`)
        }
    } catch (error) {
        next(error)
    }
}