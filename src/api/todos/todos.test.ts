import request from 'supertest';

import app from '../../app';
import { Todos } from './todos.model';

beforeAll(async () => {
    try {
        await Todos.drop()
    } catch (error) {
        console.log(error);
    }
})

describe('GET /api/v1/todos', () => {
    it('responds with an array of todos', async () =>
        request(app)
            .get('/api/v1/todos')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .then(res => {
                expect(res.body).toHaveProperty('length')
                expect(res.body.length).toBe(0)
            })
    );
});

let id = ''

describe('POST /api/v1/todos', () => {
    it('responds with an inserted object', async () =>
        request(app)
            .post('/api/v1/todos')
            .set('Accept', 'application/json')
            .send({ content: 'learn TS', done: true })
            .expect('Content-Type', /json/)
            .expect(201)
            .then(res => {
                expect(res.body).toHaveProperty('_id')
                id = res.body._id
                expect(res.body).toHaveProperty('content')
                expect(res.body).toHaveProperty('done')
            })
    );
});


describe('GET /api/v1/todos:id', () => {
    it('responds with a single todo', async () =>
        request(app)
            .get(`/api/v1/todos/${id}`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .then(res => {
                expect(res.body).toHaveProperty('_id')
                expect(res.body._id).toBe(id)
                expect(res.body).toHaveProperty('content')
                expect(res.body).toHaveProperty('done')
            })
    );
    it('responds with a not found error', async () => {
        request(app)
            .get(`/api/v1/todos/dsadsadasdsadasda`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)

    });
});