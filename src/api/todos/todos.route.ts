import { Router } from "express";

import * as TodoHandlers from './todos.handlers'
import { validateRequest } from "../../middlewares";
import { Todo } from "./todos.model";
import { ParamsWithId } from "../../interfaces/ParamsWithId";


const router = Router();

router.get('/', TodoHandlers.findAll);
router.get('/', TodoHandlers.findOne);
router.post('/', TodoHandlers.createOne);

// validateRequest({ params: ParamsWithId }),
// validateRequest({ body: Todo }),
export default router